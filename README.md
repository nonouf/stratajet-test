# README #

This application requires CocoaPods to install the dependencies.
If CocoaPods is not installed on your system, open a terminal and run the following command:

```
sudo gem install cocoapods --pre
```

Before running the app the first time, follow these instructions:

1.  Open a terminal
1. Navigate where the project is located on your computer
1. Run the command "pod install"
1. Open the file Stratajet-Test.xcworkspace
1. You can now run the application properly


If you encounter any problems with this project, feel free to send me an e-mail to arnaud.schildknecht@epitech.eu