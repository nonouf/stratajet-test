//
//  main.m
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
