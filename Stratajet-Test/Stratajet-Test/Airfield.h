//
//  Airfield.h
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol Airfield
@end

@interface Airfield : JSONModel

@property (strong, nonatomic) NSString  *city;
@property (strong, nonatomic) NSString  *createdAt;
@property (strong, nonatomic) NSString  *icaoCode;
@property (assign, nonatomic) int       airfieldID;
@property (strong, nonatomic) NSNumber  *latitude;
@property (strong, nonatomic) NSNumber  *longitude;
@property (strong, nonatomic) NSString  *name;
@property (strong, nonatomic) NSString  *updatedAt;

@end
