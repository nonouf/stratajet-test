//
//  MasterViewController.m
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import "AirfieldsViewController.h"
#import "AirfieldDetailViewController.h"
#import "RemoteCallManager.h"
#import "Airfield.h"
#import <CocoaLumberjack/CocoaLumberjack.h>
#import <CocoaLumberjack/DDLegacyMacros.h>

#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_ERROR;
#endif

@interface AirfieldsViewController ()

@property UIColor          *cellBackgroundColor;
@property NSMutableArray  *airfields;

@end

@implementation AirfieldsViewController

- (void)awakeFromNib {
  [super awakeFromNib];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  _cellBackgroundColor = [UIColor colorWithRed:243.0 / 255.0 green:243.0 / 255.0 blue:243.0 / 255.0 alpha:1.0];
  
  [self.view setBackgroundColor: _cellBackgroundColor];
  
  UINavigationBar *navBar = self.navigationController.navigationBar;
  
  navBar.tintColor = [UIColor whiteColor];
  [navBar setBarTintColor:[UIColor blackColor]];
  [navBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
  
  RemoteCallManager   *rcm = [[RemoteCallManager alloc] init];

  [rcm getAirfieldsWithSuccessBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
    
    for (NSDictionary *airfieldObj in responseObject) {
      Airfield    *airfield = [[Airfield alloc] initWithDictionary:airfieldObj error:nil];
      
      [self insertNewObject:airfield];
    }
    
    NSSortDescriptor  *descriptor = [[NSSortDescriptor alloc] initWithKey:@"icaoCode" ascending:YES];
    
    [self.airfields sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
  } andFailureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
    DDLogError(@"Error: %@", error.description);
    
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"An error occurred"
                                                         message:@"Can't retrieve airfields list. Sorry for the inconvenience."
                                                        delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    
    [errorAlert show];
  }];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(Airfield*)airfield {
  if (!self.airfields) {
    self.airfields = [[NSMutableArray alloc] init];
  }
  [self.airfields insertObject:airfield atIndex:0];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([[segue identifier] isEqualToString:@"showDetail"]) {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Airfield    *airfield = self.airfields[indexPath.row];
    
    [[segue destinationViewController] setAirfield:airfield];
  }
}

- (UINavigationItem *)navigationItem {
  UINavigationItem *item = [super navigationItem];
  
  if (item != nil && item.backBarButtonItem == nil) {
    item.backBarButtonItem = [[UIBarButtonItem alloc] init];
    item.backBarButtonItem.title = @"";
  }
  
  return item;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.airfields.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
  
  Airfield *airfield = self.airfields[indexPath.row];
  
  cell.backgroundColor = _cellBackgroundColor;
  cell.textLabel.text = [airfield icaoCode];
  cell.textLabel.textColor = [UIColor grayColor];
  cell.detailTextLabel.text = [airfield name];
  cell.detailTextLabel.textColor = [UIColor blackColor];
  return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
  // Return NO if you do not want the specified item to be editable.
  return NO;
}

@end
