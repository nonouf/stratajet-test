//
//  RemoteCallManager.h
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface RemoteCallManager : NSObject

/**
 *  Get all airfields
 */
- (void)getAirfieldsWithSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     andFailureBlock: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

@end
