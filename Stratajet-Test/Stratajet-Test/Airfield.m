//
//  Airfield.m
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import "Airfield.h"

@implementation Airfield

+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                     @"created_at": @"createdAt",
                                                     @"icao_code": @"icaoCode",
                                                     @"id": @"airfieldID",
                                                     @"lat": @"latitude",
                                                     @"long": @"longitude",
                                                     @"updated_at": @"updatedAt",
                                                     }];
}

@end
