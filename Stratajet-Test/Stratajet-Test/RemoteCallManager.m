//
//  RemoteCallManager.m
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import "RemoteCallManager.h"

@implementation RemoteCallManager

static NSString   *BASE_URL = @"http://airfield-api.herokuapp.com/airfields?token=34c922b8e948cb02dd7463ea91ef0a398c81c93362373071034be2d5f2703bf748d38bbd80f659ae9cfbcbb88f07a6261a7cb87dbe71bfef99298728f68ac7ee";

- (void)makeGetCallWithStringURL: (NSString*)stringURL
                 andSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                 andFailureBlock: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
  AFHTTPRequestOperationManager *httpManager = [AFHTTPRequestOperationManager manager];
  
  [httpManager GET:stringURL parameters:nil success:success failure:failure];
}


- (void)getAirfieldsWithSuccessBlock: (void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                     andFailureBlock: (void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
  
  [self makeGetCallWithStringURL:BASE_URL andSuccessBlock:success andFailureBlock:failure];
}


@end
