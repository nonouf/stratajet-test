//
//  DetailViewController.m
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import "AirfieldDetailViewController.h"

@interface AirfieldDetailViewController ()

@end

@implementation AirfieldDetailViewController

- (void)setAirfield:(Airfield *)airfield {
  if (_airfield != airfield) {
    _airfield = airfield;
    
    // Update the view.
    [self configureView];
  }
}

- (void)configureView {
  self.view.backgroundColor = [UIColor colorWithRed:243.0 / 255.0 green:243.0 / 255.0 blue:243.0 / 255.0 alpha:1.0];
  
  if (self.airfield) {
    self.airfieldName.text = [NSString stringWithFormat:@"%@ - %@", [self.airfield icaoCode], [self.airfield name]];
    self.cityName.text = [self.airfield city];
    
    CLLocationCoordinate2D  locationPoint;
    MKPointAnnotation       *location = [[MKPointAnnotation alloc] init];
    MKCoordinateRegion      region;
    
    locationPoint.latitude = [[self.airfield latitude] doubleValue];
    locationPoint.longitude = [[self.airfield longitude] doubleValue];
    location.title = [self.airfield name];
    location.coordinate = locationPoint;
    region = MKCoordinateRegionMakeWithDistance(locationPoint, 5000, 5000);
    region.center = locationPoint;
    
    [self.airfieldMap addAnnotation:location];
    [self.airfieldMap setRegion:[self.airfieldMap regionThatFits:region] animated:YES];
  }
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
  
  [self.view setAutoresizesSubviews:YES];
  
  UIColor *textColor = [UIColor colorWithRed:68.0 / 255.0 green:123.0 / 255.0 blue:172.0 / 255.0 alpha:1.0];

  [self.airfieldTitle setTextColor:textColor];
  [self.airfieldName setTextColor:textColor];
  [self.cityTitle setTextColor:textColor];
  [self.cityName setTextColor:textColor];
  [self.mapTitle setTextColor:textColor];
  
  [self configureView];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
