//
//  DetailViewController.h
//  Stratajet-Test
//
//  Created by Arnaud Schildknecht on 15/06/15.
//  Copyright (c) 2015 Arnaud Schildknecht. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Airfield.h"

@interface AirfieldDetailViewController : UIViewController <MKMapViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *airfieldTitle;
@property (weak, nonatomic) IBOutlet UILabel *airfieldName;
@property (weak, nonatomic) IBOutlet UILabel *cityTitle;
@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *mapTitle;
@property (weak, nonatomic) IBOutlet MKMapView *airfieldMap;


@property (strong, nonatomic) Airfield        *airfield;

@end

